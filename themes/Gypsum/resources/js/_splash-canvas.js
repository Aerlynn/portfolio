class SplashCanvas {
    constructor (dom, type, settings) {
        this.data = {};
        this.animationStart;
        this.images = {};

        this.settings = Object.assign({
            fpslimit: 60,
            bufferCanvas: 4 // -1 = no canvas, 2 = 1/2 size, 4 = 1/4 size
        }, settings);

        this.initCanvas(dom);
        this.initAnimation(type);
    }

    initCanvas (dom) {
        this.canvas = dom;
        this.ctx = this.canvas.getContext('2d');

        if (this.settings.bufferCanvas !== -1) {
            this.bufferCanvas = document.createElement('canvas');
            this.bufferCanvas.id = (this.canvas.id) ? this.canvas.id + "-buffer" : "splash-canvas-buffer";
            this.bufferCanvas.classList.add("splash-canvas");
            //this.canvas.parentElement.appendChild(this.bufferCanvas);
            
            //this.canvas.style.opacity = 0;
            //this.bufferCanvas.style.opacity = 0;
            this.bufferCtx = this.bufferCanvas.getContext('2d');
        }

        this.resizeCanvas();
    }

    resizeCanvas () {
        let box = this.canvas.getBoundingClientRect();
        this.canvas.width = box.width;
        this.canvas.height = box.height;
        this.ctx.clearRect(0, 0, this.canvas.width, this.canvas.height);

        if (this.bufferCanvas) {
            this.bufferCanvas.width = box.width / 4;
            this.bufferCanvas.height = box.height / 4;
            this.bufferCtx.clearRect(0, 0, this.bufferCanvas.width, this.bufferCanvas.height);
        }
    }

    initAnimation (type) {
        if (type === "") type = "Teardrops";
        this.animationInit = "init" + type;
        this.animateFunc = "animate" + type;

        if (typeof this[this.animationInit] === "function") {
            this[this.animationInit]();
        }
    }

    update (timestamp) {
        if (!this.animationStart) this.animationStart = timestamp;
        let progress = timestamp - this.animationStart;
        
        requestAnimationFrame(this.update.bind(this));

        const framerate = 1000 / this.settings.fpslimit;
        let delta = (timestamp - this.lastTimestamp);
        
        if (this.settings.fpslimit) {
            if (delta < framerate) {
                return false;
            }
        }

        if (typeof this[this.animateFunc] === "function") {
            this[this.animateFunc](delta / 1000, progress);
        } 
        this.lastTimestamp = timestamp;
    }

    loadImage (fileName, callback) {
        const img = new Image();
        img.addEventListener('load', () => { callback(); })
        img.src = globals.siteUrl + "assets/" + fileName;
        return img;
    }

    initTeardrops() {
        // Loading
        this.images.teardrop = this.loadImage("teardrop_sheet.png", () => {
            if (typeof this[this.animateFunc] === "function") {
                window.requestAnimationFrame(this.update.bind(this));
            }
            else {
                console.log("No animation function found for " + this.animateFunc);
            }
        });
        /*
        this.images.teardropS = new Image();
        this.images.teardropS.addEventListener('load', () => {
            if (typeof this[this.animateFunc] === "function") {
                window.requestAnimationFrame(this.update.bind(this));
            }
            else {
                console.log("No animation function found for " + this.animateFunc);
            }
        });
        this.images.teardropS.src = globals.siteUrl + "assets/teardrop.png";
        */

        // Set up data
        this.data.teardrops = [];
        for (let i = 1; i < 18; i++) {
            setTimeout(() => {
                this.data.teardrops.push(this.generateTeardrop(this.data.teardrops.length));
            }, 2000 * i);
        }
    }

    generateTeardrop (index) {
        // Random
        const seed = globals.prng.generate();
        const z = (7 * seed + 1) / 8;

        // Settings
        const sprite = Math.floor(seed * 3);
        const image = this.images.teardrop;
        const imageSize = { width: 98 };
        imageSize.height = sprite > 0 ? 400 + (sprite * 400) : 520;

        // Generate scaled image once for optimization
        const scale = Math.pow((z + 0.2), 2) / 2.25;
        const scaledSize = { width: imageSize.width * scale, height: imageSize.height * scale }

        const teardropCanvas = document.createElement('canvas');
        teardropCanvas.width = scaledSize.width;
        teardropCanvas.height = scaledSize.height;

        const teardropCtx = teardropCanvas.getContext('2d');
        teardropCtx.clearRect(0, 0, teardropCanvas.width, teardropCanvas.height);
        teardropCtx.drawImage(image, sprite * imageSize.width, 1200 - imageSize.height, imageSize.width, imageSize.height, 0, 0, scaledSize.width, scaledSize.height);

        const scaledImage = teardropCanvas;

        // Return
        return {
            index: index,
            sprite: sprite,
            image: scaledImage,
            imageSize: scaledSize,
            x: (Math.random() * this.canvas.width) - (this.canvas.width / 2), // Center horizontally
            y: (1 + Math.random()) * -scaledSize.height, // 1x-2x from top
            z: z,
            speed: (Math.random() + 0.5) * Math.pow(z, 2) * 0.5,
            alpha: ((z + 0.3) / 1.3) - (scale / 2),
            dateOfBirth: this.lastTimestamp || 0
        };
    }

    animateTeardrops(deltaTime, progress) {
        // Clear and reset
        //this.ctx.globalAlpha = .0002;
        //this.ctx.clearRect(0, 0, this.canvas.width, this.canvas.height);

        if (this.bufferCtx) {
            this.bufferCtx.clearRect(0, 0, this.bufferCanvas.width, this.bufferCanvas.height);
            this.bufferCtx.globalAlpha =  0.75;
            this.bufferCtx.drawImage(this.canvas, 0, 0, this.bufferCanvas.width, this.bufferCanvas.height);
        }

        this.ctx.clearRect(0, 0, this.canvas.width, this.canvas.height);
        this.ctx.globalAlpha = 1;
        this.ctx.drawImage(this.bufferCanvas, 0, 0, this.canvas.width, this.canvas.height);

        // Draw
        this.data.teardrops.forEach((teardrop) => {
            const speed = (progress - teardrop.dateOfBirth) * teardrop.speed;
            this.ctx.globalAlpha = teardrop.alpha;

            const x = teardrop.x + (this.canvas.width / 2); // Center horizontally
            const y = teardrop.y + speed;
            this.ctx.drawImage(teardrop.image, x, y);

            if (y > this.canvas.height) {
                this.data.teardrops[teardrop.index] = this.generateTeardrop(teardrop.index);
            }
        });
    }
}

export default SplashCanvas;