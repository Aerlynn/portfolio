require('../sass/screen.scss');

// Stonekit
import Stonekit from '../stonekit/Stonekit';
import Stickyfill from '../stonekit/modules/stickyfill/stickyfill.js';
import Smoothscroll from '../stonekit/modules/smoothscroll/smoothscroll.js';
import Photoswipe from '../stonekit/modules/photoswipe/photoswipe.js';
import Inview from '../stonekit/modules/inview/inview.js';
//import ThreeJS from '../stonekit/modules/threejs/threejs.js'; // ~1 MB
import PropScroll from '../stonekit/modules/propscroll/propscroll.js';
import ClassScroll from '../stonekit/modules/classscroll/classscroll.js';
import DynamicHeader from '../stonekit/modules/dynamicheader/dynamicheader.js';
//import Swiper from '../stonekit/modules/swiper/swiper.js';
import SeededRandom from '../stonekit/modules/seededrandom/seededrandom.js';
import Validator from '../stonekit/modules/validator/validator.js';

// Portfolio
import SplashCanvas from './_splash-canvas.js';
import Petroglyph from './Petroglyph.js';

// App settings
const appSettings = {
    env: 'development',
    responsive: {
        mobile: 480,
        tablet: 768,
        desktop: 1024
    }
}

class App {
    constructor (settings) {
        this.settings = settings;

        // Load elements
        this.elements = {
            frame: document.querySelectorAll(".site-frame")[0],
            header: document.querySelectorAll(".site-header")[0],
            content: document.querySelectorAll(".site-container")[0],
            footer: document.querySelectorAll(".site-footer")[0]
        }
    }
    
    init () {
        // Start up
        window.requestAnimationFrame(() => {
            // Disable no-js styling
            document.documentElement.classList.remove("no-js");

            // Load transition
            this.elements.frame.classList.add("is-loaded");

            // Scroll to hash, if there is one
            if (window.location.hash && this.stonekit.uses(Smoothscroll)) {
                this.stonekit.modules.Smoothscroll.scrollToHash(window.location.hash.split("#")[1]);
            }

            // Fire scroll action once
            this.scrollHandler();
        });

        // @TODO: Image comparison slider
        // @TODO: Swiper module
        // @TODO: Form validation + form layout (CSS Grid?)
        
        // Load all desired Stonekit modules
        this.stonekit = new Stonekit({ app: appSettings });
        this.stonekit.use(Stickyfill);
        this.stonekit.use(Smoothscroll, { useHash: false });
        this.stonekit.use(Photoswipe);
        this.stonekit.use(Inview);
        this.stonekit.use(PropScroll);
        this.stonekit.use(ClassScroll);
        this.stonekit.use(DynamicHeader);
        //this.stonekit.use(Swiper);
        this.stonekit.use(Validator);
        this.prng = this.stonekit.use(SeededRandom, { seed: "stonekitSeed" });
        window.globals.prng = this.prng;
        //this.threeJsContainer = document.querySelectorAll(".js-threejs-canvas")[0];
        //if (this.threeJsContainer) { this.stonekit.use(ThreeJS, { parentNode: this.threeJsContainer, width: this.threeJsContainer.clientWidth, height: this.threeJsContainer.clientHeight }); }
        
        // console.log(this.stonekit);

        // Initialise SplashCanvas
        let splashCanvasEls = document.querySelectorAll(".js-splash-canvas");
        this.splashCanvasses = [];
        splashCanvasEls.forEach((dom) => {
            let splashCanvas = new Petroglyph(dom);
            this.stonekit.subscribeToEvent("resize", splashCanvas.resize.bind(splashCanvas), "petroglyph-guid");
            this.stonekit.subscribeToEvent("mousemove", splashCanvas.mousemove.bind(splashCanvas), "petroglyph-guid");
            this.splashCanvasses.push(splashCanvas);
        });

        // Throttled scroll event handler
        this.scrollThrottleHandler = (e) => this.throttle(e, "scroll");
        window.addEventListener("scroll", this.scrollThrottleHandler);

        // Throttled resize event handler
        this.resizeThrottleHandler = (e) => this.throttle(e, "resize");
        window.addEventListener("resize", this.resizeThrottleHandler);
        
        // Throttled resize event handler
        this.mousemoveThrottleHandler = (e) => this.throttle(e, "mousemove");
        window.addEventListener("mousemove", this.mousemoveThrottleHandler);
    }

    throttle (e, id) {
        let ref = "_" + id + "ThrottleTimeout";
        if (!this[ref]) {
            this[ref] = setTimeout(() => {
                this[ref] = null;
                this[id + "Handler"](e);
            }, 66); // 15fps
        }
    }

    scrollHandler (e) {
        const scrollY = window.pageYOffset;
        this.stonekit.callEvent("scroll", { event: e, scrollY: scrollY });
    }

    resizeHandler (e) {
        this.stonekit.callEvent("resize", { event: e });
    }

    mousemoveHandler (e) {
        this.stonekit.callEvent("mousemove", { event: e });
    }
}

(() => {
    const loadApp = () => {
        const app = new App( appSettings );
        document.removeEventListener("DOMContentLoaded", loadApp);
        app.init();
    }

    if (document.readyState === "complete") { document.addEventListener("DOMContentLoaded", loadApp); }
    else { setTimeout( loadApp(), 1) }
})()