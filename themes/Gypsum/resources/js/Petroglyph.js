class Petroglyph {
    constructor (canvas, settings) {
        // Imports
        this.canvas = canvas;
        this.settings = Object.assign({
            fpslimit: 60,
            timescale: 1,
            padding: {
                x: 400,
                y: 400
            },
            fillAlpha: 0.015,
            strokeAlpha: 0.06,
            colorSetDuration: 4000
        }, settings);

        // Declarations
        this.data = {};
        this.images = {};
        this.cursor = {
            x: 0,
            y: 0
        };

        this.startTime;
        this.timestamp;
        this.lastTimestamp;
        this.progress;

        //
        if (this.canvas.getContext) {
            this.ctx = this.canvas.getContext('2d');
    
            this.init();
        }
    }

    // @Todo: Move to Utils
    loadImages (srcArray) {
        var imagesLoaded = [];
        srcArray.forEach((src) => {
            imagesLoaded.push(new Promise((resolve, reject) => {
                this.loadImage(src).then(img => resolve(img));
            }));
        });
        return Promise.all(imagesLoaded);
    }
    loadImage (src) {
        return new Promise((resolve, reject) => {
            const img = new Image();
            img.addEventListener('load', () => {
                resolve(img);
            });
            img.src = src;
        });
    }
    /*
    loadImage (src, callback) {
        const img = new Image();
        img.addEventListener('load', () => {
            img.removeEventListener('load');
            callback();
        });
        img.src = src;
        return img;
    }
    */

    init () {
        this.resize();
        this.load();

        this.test(-2);
        this.test(-1);
        this.test(0);
        this.test(1);
        this.test(2);
    }

    test(l) {
        //console.log(Math.pow(l, 2) + (l * 2));
    }

    resize (e) {
        const box = this.canvas.getBoundingClientRect();
        this.canvas.width = box.width;
        this.canvas.height = box.height;
        this.ctx.clearRect(0, 0, this.canvas.width, this.canvas.height);

        // @todo: move distance to util func
        this.maxDistance = this.distance2d(this.canvas.width, this.canvas.height);

        // Random color gen, but start with preset colors
        this.toCSet = this.generateColorSet(this.settings.colorSetDuration / 2); // Interpolates to this color set
        
        // Color 1
        this.cSet = { r: 0, g: 255, b: 255, time: 0, contrast: 0.5 };
        for (let i=0; i<600; i++) {
            this.render(i * 0.1);
        }

        // Color 2
        this.cSet = { r: 255, g: 0, b: 50, time: 0, contrast: 0.5 };
        for (let i=0; i<200; i++) {
            this.render(i * 0.1);
        }

        // Start animation colors
        this.cSet = { r: 50, g: 0, b: 0, time: 0, contrast: 0.5 };
        this.toCSet = { r: 250, g: 250, b: 90, time: this.settings.colorSetDuration };
    }

    mousemove (e) {
        // @todo: translate e.event positions to canvas dom element (works now because it's fullscreen)
        if (!this.cursor.lastX) this.cursor.lastX = this.cursor.x;
        if (!this.cursor.lastY) this.cursor.lastY = this.cursor.y;
        this.cursor.lastUpdate = this.progress;
        this.cursor.x = e.event.clientX;
        this.cursor.y = e.event.clientY;
    }

    generateColorSet(timeOffset = 0) {
        const set = {
            r: Math.round(Math.random() * 255),
            g: Math.round(Math.random() * 255),
            b: Math.round(Math.random() * 255),
            time: this.timestamp + timeOffset || timeOffset,
            contrast: Math.round(Math.random() * 100)
        };
        return set;
    }

    update (timestamp) {
        if (!this.startTime) this.startTime = timestamp;
        this.timestamp = timestamp;
        this.progress = (this.timestamp - this.startTime) * this.settings.timescale;
        
        requestAnimationFrame(this.update.bind(this));

        const delta = (timestamp - this.lastTimestamp);
        
        // Framerate limit
        const framerate = 1000 / this.settings.fpslimit;
        if (this.settings.fpslimit) {
            if (delta < framerate) {
                return false;
            }
        }
        // Render
        this.render(delta / 1000);
        this.lastTimestamp = timestamp;
    }

    // Defined in instance:
    load() {
        // Loading
        // this.images.teardrop = this.loadImage(globals.siteUrl + "assets/teardrop_sheet.png", () => {
        //     window.requestAnimationFrame(this.update.bind(this));
        // });

        /*
        this.loadImage(globals.siteUrl + "assets/mask_1.png").then((img) => {
            this.img = img;
        });
        */
            this.update();
    }

    render(deltaTime) {
        // Clear and reset
        this.ctx.globalCompositeOperation = 'source-over';
        // this.ctx.clearRect(0, 0, this.canvas.width, this.canvas.height);

        this.ctx.fillStyle = "rgb(0, 0, 0)";
        this.ctx.globalAlpha = 0.005;
        this.ctx.fillRect(0, 0, this.canvas.width, this.canvas.height);

        // Change color set over time
        if (this.timestamp > this.toCSet.time) {
            this.cSet = this.toCSet;
            this.toCSet = this.generateColorSet(this.settings.colorSetDuration);
        }

        // Points for triangles
        const pts = {
            x1: (Math.random() * (this.canvas.width + (this.settings.padding.x * 2))) - (this.settings.padding.x * 1.3),
            x2: (Math.random() * (this.canvas.width + (this.settings.padding.x * 2))) - (this.settings.padding.x * 1.3),
            x3: (Math.random() * (this.canvas.width + (this.settings.padding.x * 2))) - (this.settings.padding.x * 1.3),
            y1: (Math.random() * (this.canvas.height + (this.settings.padding.y * 2))) - (this.settings.padding.y * 1.3),
            y2: (Math.random() * (this.canvas.height + (this.settings.padding.y * 2))) - (this.settings.padding.y * 1.3),
            y3: (Math.random() * (this.canvas.height + (this.settings.padding.y * 2))) - (this.settings.padding.y * 1.3)
        }

        // Averages
        const a = {
            x: pts.x1 + pts.x2 + pts.x3 / 3,
            y: pts.y1 + pts.y2 + pts.y3 / 3
        }
        a.both = this.distance2d(a.x, a.y);

        // Colors + contrast
        const luma = (this.cSet.r + this.cSet.r + this.cSet.g + this.cSet.g + this.cSet.g + this.cSet.b) / (6 * 255); // https://stackoverflow.com/questions/596216/formula-to-determine-brightness-of-rgb-color
        const toLuma = (this.toCSet.r + this.toCSet.r + this.toCSet.g + this.toCSet.g + this.toCSet.g + this.toCSet.b) / (6 * 255);
        const lumaDiff = ((toLuma - luma) * 0.5) + 1; // Halved difference with factor 1 as base
        const alphaFactor = Math.max(1, (Math.pow((luma), 2) * 0.5));
        //console.log(Math.round(luma * 100), alphaFactor);
        const c = {
            ...this.cSet,
            r: this.cSet.r * lumaDiff,
            g: this.cSet.g * lumaDiff,
            b: this.cSet.b * lumaDiff
        };

        // Randomize values within colorset
        const d = [
            (a.x / this.canvas.width),
            (a.x / this.canvas.width),
            (a.y / this.canvas.height),
            (a.both / this.maxDistance),
            (a.y / this.canvas.width),
            (a.x / this.canvas.height)
        ];
        const drr = Math.round(Math.random() * d.length);
        const dr = [
            Math.round(Math.random() * d.length),
            Math.round(Math.random() * d.length),
            Math.round(Math.random() * d.length)
        ];

        // DRAW
        this.ctx.globalAlpha = this.settings.fillAlpha; // * alphaFactor;

        //this.ctx.fillStyle = "rgb(" + Math.round(d[dr[0]] * c.r) + ", " + Math.round(d[dr[1]] * c.g) + ", " + Math.round(d[dr[2]] * c.b) + ")";
        this.ctx.fillStyle = "rgb(" + Math.round(d[drr] * c.r) + ", " + Math.round(d[drr] * c.g) + ", " + Math.round(d[drr] * c.b) + ")";


        // this.ctx.fillStyle = "rgb(122, " + Math.round(d[dr[1]] * c.g) + ", " + Math.round(d[dr[2]] * c.b) + ")";
        //this.ctx.fillStyle = "rgb(" + Math.round(d[dr[0]] * c.r) + ", " + Math.round(d[dr[1]] * c.g) + ", 122)";
        
        this.ctx.beginPath();
        this.ctx.moveTo(pts.x1, pts.y1);
        this.ctx.lineTo(pts.x2, pts.y2);
        this.ctx.lineTo(pts.x3, pts.y3);
        this.ctx.fill();
        
        
        this.ctx.globalAlpha = this.settings.strokeAlpha;
        this.ctx.strokeStyle = "rgb(" + Math.round(d[dr[0]] * c.r * 1.5) + ", " + Math.round(d[dr[1]] * c.g * 1.5) + ", " + Math.round(d[dr[2]] * c.b * 1.5) + ")";
        this.ctx.beginPath();
        this.ctx.moveTo(pts.x1, pts.y1);
        this.ctx.lineTo(pts.x2, pts.y2);
        this.ctx.lineTo(pts.x2 + 1, pts.y2 + 1);
        this.ctx.stroke();


        // this.ctx.globalCompositeOperation = 'destination-out';
        // this.ctx.drawImage(this.img, 0, 0.53 * this.canvas.height, 0.294 * this.canvas.height, 0.4 * this.canvas.height);


        /*
        if (this.cursor.lastUpdate) {
            const lerpDuration = 1000; // ms
            const lerpProgress = Math.round(Math.min(this.progress - this.cursor.lastUpdate, lerpDuration)) / 1000;
            const lerpCursor = {
                x: ((this.cursor.x - this.cursor.lastX) * lerpProgress) + this.cursor.lastX,
                y: ((this.cursor.y - this.cursor.lastY) * lerpProgress) + this.cursor.lastY,
            };
            // Distance -- @todo: create distance Utils func
            const distance = this.distance2d(lerpCursor.x - this.cursor.x, lerpCursor.y, this.cursor.y);
            
            this.ctx.globalAlpha = 1;
            this.ctx.lineWidth = Math.max(Math.min(Math.round((distance / this.maxDistance) * 50), 20), 6);

            this.ctx.beginPath();
            this.ctx.moveTo(this.cursor.lastX, this.cursor.lastY);
            this.ctx.lineTo(lerpCursor.x, lerpCursor.y);
            this.ctx.closePath();
            this.ctx.stroke();


            this.cursor.lastX = lerpCursor.x;
            this.cursor.lastY = lerpCursor.y;

            // this.ctx.fillRect(lerpCursor.x - 12, lerpCursor.y - 12, 24, 24);
        }
        */
    }

    // @todo: Move to utils
    distance2d (a, b) {
        return Math.sqrt(Math.pow(a, 2) + Math.pow(b, 2));
    }
}

export default Petroglyph;